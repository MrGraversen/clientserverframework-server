package task;

import helpers.ConsoleHelper;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import network.*;
import client.*;
import exceptions.OwnzoneException;

/**
 * This class runs asynchronously and checks the following in intervals:
 * <ul>
 * <li>Are any new tasks pending: If so, prepare them for execution and execute.</li>
 * <li>Are any tasks done execution: If so, prepare them for delivery back to the client.</li>
 * <li>Are any tasks waiting to be sent: If so, send them back to the client.</li>
 * </ul>
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class TaskExecutor extends Thread
{
	private final String NAME = this.getClass().getSimpleName();

	private final static int THROTTLE_VALUE = 10;

	private volatile static TaskExecutor taskExecutorInstance = null;

	private TaskQueue taskQueue;

	private ExecutorService executor;

	private volatile boolean stop;

	private ClientManager clientManager;

	/**
	 * Instantiates a new task executor, preparing its resource for use.
	 */
	private TaskExecutor()
	{
		// executor = Executors.newFixedThreadPool(n);
		executor = Executors.newCachedThreadPool();
		taskQueue = TaskQueue.getTaskQueueInstance();
		clientManager = ClientManager.getClientManagerInstance();
		stop = false;
		ConsoleHelper.announce(NAME, "Ready.");
	}

	/**
	 * Gets the singleton task executor instance.
	 *
	 * @return the task executor instance
	 */
	public static TaskExecutor getTaskExecutorInstance()
	{
		if (taskExecutorInstance == null) taskExecutorInstance = new TaskExecutor();

		return taskExecutorInstance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	public void run()
	{
		while (!stop)
		{
			pollRequestedTasks();
			pollExecutingTasks();
			pollCompletedTasks();
			hibernate(THROTTLE_VALUE);
		}
	}

	/**
	 * Poll requested tasks. If any tasks are waiting, prepare them for execution and execute them.
	 */
	private void pollRequestedTasks()
	{
		TaskExecutionEnvironment environment = taskQueue.getNextRequestedTask();

		if (environment != null)
		{
			ConsoleHelper.announce(NAME, "Executing task: " + environment.getTask().getIdentifier());

			Client client = clientManager.getClient(environment.getClientId());
			environment.putClientData(client);
			environment.putSharedMemorySnapshot(clientManager.getSharedDataBag());

			TaskBase task = environment.prepareExecute();

			taskQueue.putExecutingTask(environment);
			executor.execute(task);
		}
	}

	/**
	 * Poll executing tasks. If any of the executing tasks are completed, prepare them for delivery back to the client.
	 */
	private void pollExecutingTasks()
	{
		TaskExecutionEnvironment environment = taskQueue.getNextExecutingTask();

		if (environment != null)
		{
			taskQueue.completeTask(environment);
		}
	}

	/**
	 * Poll completed tasks. If any tasks are waiting, build a reply and send it back to the client.
	 */
	private void pollCompletedTasks()
	{
		TaskExecutionEnvironment environment = taskQueue.getNextCompletedTask();

		if (environment != null)
		{
			NetworkPackage networkPackage;
			Client client = clientManager.getClient(environment.getClientId());

			clientManager.addSharedData(environment.getSharedDataAdditions());

			if (environment.success())
			{
				networkPackage = NetworkPackageFactory.buildNetworkPackage(environment);
			}
			else
			{
				if (environment.getException() == null)
				{
					networkPackage = NetworkPackageFactory.buildNetworkPackage(new OwnzoneException("Task execution failed safely (no unhandled exception was thrown)"), environment);
				}
				else
				{
					networkPackage = NetworkPackageFactory.buildNetworkPackage(environment.getException(), environment);
				}
			}

			if (client != null)
			{
				client.reply(networkPackage);
				client.copyFromEntity(environment.getNewClientDataBag());
			}
			else
			{
				ConsoleHelper.announce(NAME, "Looks like client was dropped while a task was executing; discarding reply.");
			}
		}
	}

	/**
	 * Sleep for the specified duration.
	 *
	 * @param duration
	 *            the duration
	 */
	private void hibernate(int duration)
	{
		try
		{
			Thread.sleep((long) duration);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Abort the thread, stopping the asynchronous execution.
	 */
	public void abort()
	{
		stop = true;
	}
}