package task;

import helpers.ConsoleHelper;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import util.*;
import exceptions.OwnzoneException;

/**
 * This class manages collections of tasks in their different states. These include:
 * <ul>
 * <li>Requested tasks: Tasks that are scheduled for execution but not actually executing yet.</li>
 * <li>Executing tasks: Tasks that are currently executing.</li>
 * <li>Completed tasks: Tasks that has completed execution and is pending delivery to the client.</li>
 * </ul>
 * This class also delivers metric information to the {@link MetricObserver} in intervals.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class TaskQueue
{
	private final String NAME = this.getClass().getSimpleName();

	private static TaskQueue taskQueueInstance;

	private Map<String, TaskBase> registeredTasks;

	private Queue<TaskExecutionEnvironment> requestedTasks;

	private Queue<TaskExecutionEnvironment> completedTasks;

	private Queue<TaskExecutionEnvironment> executingTasks;

	private MetricObserver metricObserver;

	/**
	 * Instantiates a new task queue, preparing the resources for use and preparing the {@link MetricObserver} instance for use.
	 */
	private TaskQueue()
	{
		registeredTasks = new ConcurrentHashMap<String, TaskBase>();

		requestedTasks = new LinkedList<TaskExecutionEnvironment>();
		completedTasks = new LinkedList<TaskExecutionEnvironment>();
		executingTasks = new LinkedList<TaskExecutionEnvironment>();

		// TaskQueue tq = TaskQueue.getTaskQueueInstance();
		// tq.registerTask(new ConsoleTask());

		metricObserver = MetricObserver.getMetricObserverInstance();

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run()
			{
				updateMetrics();
			}
		}, 1000, 5000);

		ConsoleHelper.announce(NAME, "Ready.");
	}

	/**
	 * Update metrics if the delay duration has elapsed. <br>
	 * This generates the HTML document that constitutes the "pretty" log.
	 */
	private void updateMetrics()
	{
		if (metricObserver.shouldLogUpdate())
		{
			// TODO: pass queue sizes
			metricObserver.generateHtmlMetrics();
		}
	}

	/**
	 * Gets the singleton task queue instance.
	 *
	 * @return the task queue instance
	 */
	public static synchronized TaskQueue getTaskQueueInstance()
	{
		if (taskQueueInstance == null) taskQueueInstance = new TaskQueue();

		return taskQueueInstance;
	}

	/**
	 * Register task. This allows the task to be requested by a client and executed by the server.
	 *
	 * @param task
	 *            the task
	 */
	public void registerTask(TaskBase task)
	{
		if (registeredTasks.get(task.getIdentifier()) == null)
		{
			registeredTasks.put(task.getIdentifier(), task);
			metricObserver.addObservedTask(task.getIdentifier());
			ConsoleHelper.announce(NAME, "Registered task: " + task.getIdentifier());
		}
	}

	/**
	 * Unregister task and remove every scheduled job for the task.
	 *
	 * @param identifier
	 *            the identifier
	 */
	public void unregisterTask(String identifier)
	{
		TaskBase tb = registeredTasks.remove(identifier);

		if (tb != null)
		{
			ConsoleHelper.announce(NAME, "Unregistered task: " + identifier);

			synchronized (requestedTasks)
			{
				for (Iterator<TaskExecutionEnvironment> it = requestedTasks.iterator(); it.hasNext();)
				{
					TaskExecutionEnvironment te = (TaskExecutionEnvironment) it.next();

					if (te.getTask().getIdentifier().equals(identifier))
					{
						it.remove();
						// TODO: Send message to client that task was unregistered(?)
					}
				}
			}
		}
	}

	/**
	 * Gets the next requested task.
	 *
	 * @return the next requested task
	 */
	public TaskExecutionEnvironment getNextRequestedTask()
	{
		synchronized (requestedTasks)
		{
			if (requestedTasks.peek() != null)
			{
				return requestedTasks.poll();
			}
			else
			{
				return null;
			}
		}
	}

	/**
	 * Adds a task request. When the task is added to the request queue, it will be picked up by the {@link TaskExecutor} instance as soon as possible.
	 *
	 * @param clientId
	 *            the client id
	 * @param packageId
	 *            the package id
	 * @param taskIdentifier
	 *            the task identifier
	 * @param data
	 *            the data sent from the client (if any)
	 * @throws OwnzoneException
	 *             an exception indicating an error with scheduling the task
	 */
	public void addTaskRequest(UUID clientId, UUID packageId, String taskIdentifier, ArrayList<DynamicProperty<?>> data) throws OwnzoneException
	{
		synchronized (requestedTasks)
		{
			TaskBase tb;
			try
			{
				tb = registeredTasks.get(taskIdentifier).clone();
			}
			catch (NullPointerException e)
			{
				OwnzoneException ee = new OwnzoneException("Task " + taskIdentifier + " was not found.");
				TaskExecutionEnvironment tee = new TaskExecutionEnvironment();
				tee.finalize(clientId, packageId, null, null);
				tee.complete(false, ee);
				completeTask(tee);
				throw ee;
			}

			if (tb != null)
			{
				tb.finalize(clientId, packageId, tb, data);

				requestedTasks.offer(tb);
				ConsoleHelper.announce(NAME, "Scheduling task: " + taskIdentifier + " (" + packageId + " from client " + clientId + ")");

				metricObserver.updateClientTaskRequests(clientId, taskIdentifier);
			}
			else
			{
				throw new OwnzoneException("Task " + taskIdentifier + " was not found.");
			}
		}
	}

	/**
	 * Request queue size.
	 *
	 * @return the size of the queue
	 */
	public int requestQueueSize()
	{
		return requestedTasks.size();
	}

	/**
	 * Gets the next completed task.
	 *
	 * @return the next completed task
	 */
	public TaskExecutionEnvironment getNextCompletedTask()
	{
		synchronized (completedTasks)
		{
			if (completedTasks.peek() != null)
			{
				return completedTasks.poll();
			}
			else
			{
				return null;
			}
		}
	}

	/**
	 * Gets the next executing task that has completed.
	 *
	 * @return the next executing task
	 */
	public TaskExecutionEnvironment getNextExecutingTask()
	{
		synchronized (executingTasks)
		{
			TaskExecutionEnvironment te = executingTasks.peek();
			// if ((tr != null) && (tr.getTask().getHostRequest().hasCompleted()))
			// {
			// return executingTasks.poll().getTask().getHostRequest();
			// }
			if ((te != null) && (te.getTask().hasCompleted()))
			{
				return executingTasks.poll();
			}
			else
			{
				return null;
			}
		}
	}

	/**
	 * Complete a task, scheduling it for delivery back to the client.
	 *
	 * @param environment
	 *            the task execution environment
	 */
	public void completeTask(TaskExecutionEnvironment environment)
	{
		synchronized (completedTasks)
		{
			completedTasks.add(environment);
			
			if(environment.getTask() == null) return;

			ConsoleHelper.announce(NAME, "Completed task: " + environment.getTask().getIdentifier() + " (" + environment.getExecuteDuration() + " ms)");
			metricObserver.updateTaskMetrics(environment.getTask().getIdentifier(), (int) environment.getExecuteDuration());
		}
	}

	/**
	 * Put a task into the executing-queue, letting it remain until it has completing execution.
	 *
	 * @param taskRequest
	 *            the task execution environment
	 */
	public void putExecutingTask(TaskExecutionEnvironment taskRequest)
	{
		synchronized (executingTasks)
		{
			executingTasks.add(taskRequest);
		}
	}
}
