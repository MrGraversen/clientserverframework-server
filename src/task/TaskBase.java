package task;
import java.util.ArrayList;

import mysql.DatabaseConnection;
import util.*;
import abstracts.*;
import exceptions.OwnzoneException;

/**
 * This class enables the developer to create tasks for execution on the server. This class provides basic tools as well as an execution environment for the server's task executor.
 * <br>Each task has an identifier used by client-implementations to request execution of the specific task.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public abstract class TaskBase extends TaskExecutionEnvironment implements Manageable<String>, Runnable, Cloneable
{
	private String identifier;
	
	private volatile TaskExecutionEnvironment executionEnvironment;
	
	private volatile boolean running;
	
	private volatile boolean hasStarted;

	/**
	 * Instantiates a new task.
	 *
	 * @param identifier the task's identifier - used to request the task for execution from the client.
	 */
	public TaskBase(String identifier)
	{
		this.running = false;
		this.hasStarted = false;
		this.identifier = identifier;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		running = true;

		beforeTask();

		boolean success = false;
		OwnzoneException exception = null;

		try
		{
			hasStarted = true;
			success = task();
		}
		catch (Exception e)
		{
			exception = new OwnzoneException("Task failed during execution", e);
			success = false;
		}

		afterTask();

		running = false;

		executionEnvironment.complete(success, exception);
	}

	/**
	 * Sets the execution environment, providing tools and context for the task.
	 *
	 * @param executionEnvironment the execution environment
	 */
	public void setEnvironment(TaskExecutionEnvironment executionEnvironment)
	{
		this.executionEnvironment = executionEnvironment;
	}

	/**
	 * The block of code that is the task itself.
	 *
	 * @return a boolean indicating successful execution
	 * @throws OwnzoneException the ownzone exception
	 */
	protected abstract boolean task() throws OwnzoneException;

	/**
	 * This method is called before {@code task()}<br>
	 * Possibly useful for pre-task construction.
	 */
	protected abstract void beforeTask();

	/**
	 * This method is called after {@code execute()}<br>
	 * Possibly useful for post-task deconstruction.
	 */
	protected abstract void afterTask();

	/* (non-Javadoc)
	 * @see abstracts.Manageable#getIdentifier()
	 */
	public String getIdentifier()
	{
		return identifier;
	}

	/* (non-Javadoc)
	 * @see task.TaskExecutionEnvironment#hasCompleted()
	 */
	public boolean hasCompleted()
	{
		return !running && hasStarted;
	}

	/**
	 * Adds a property to the bag of result objects.
	 * <br>The object will be sent back to the client after the task execution has completed.
	 *
	 * @param property the property
	 */
	protected void addResultData(DynamicProperty<?> property)
	{
		executionEnvironment.getResultBag().putProperty(property);
	}

	/**
	 * Adds a property to the bag of client objects.
	 * <br>The object will be added to the client's object pool after the task execution has completed, which can then be accessed from another task from the same client.
	 *
	 * @param property the property
	 */
	protected void addClientData(DynamicProperty<?> property)
	{
		executionEnvironment.getNewClientDataBag().putProperty(property);
	}

	/**
	 * Adds a property to the bag of shared objects.
	 * <br>The object is added to a pool of shared objects, accessible by all clients during all task executions. 
	 * @param property the property
	 */
	protected void addSharedData(DynamicProperty<?> property)
	{
		executionEnvironment.getSharedDataAdditions().putProperty(property);
	}

	/**
	 * Gets the client property bag.
	 * <br>This data is accessible at every execution environment for the client's tasks.
	 *
	 * @return the client data
	 */
	protected PropertyBag getClientData()
	{
		return executionEnvironment.getClientDataBag();
	}

	/**
	 * Gets the shared property bag.
	 * <br>This data is accessible by all clients' tasks in their execution environment.
	 *
	 * @return the shared data
	 */
	protected PropertyBag getSharedData()
	{
		return executionEnvironment.getSharedDataSnapshot();
	}

	/**
	 * Gets a database connection by its identifier.
	 *
	 * @param identifier the identifier
	 * @return the database connection
	 */
	protected DatabaseConnection getDatabaseConnection(String identifier)
	{
		return executionEnvironment.getDatabase(identifier);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public TaskBase clone()
	{
		try
		{
			return (TaskBase) super.clone();
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	/**
	 * Gets the host environment for this task.
	 *
	 * @return the host environment
	 */
	public TaskExecutionEnvironment getHostEnvironment()
	{
		return executionEnvironment;
	}
}
