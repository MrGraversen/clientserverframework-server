package task;

import helpers.DateTimeHelper;

import java.util.*;

import mysql.*;
import util.*;
import abstracts.DynamicPropertyEntity;
import exceptions.OwnzoneException;

/**
 * This class provides contextual tools and data for a task during execution time. This aids developers of applications to easily manage properties of transactions and client.
 * <br>This class holds the following contextual data:
 * <ul>
 * <li>Result data: The data to be sent back to the client, typically the results from execution of a task</li>
 * <li>Client data: The data pool for the client executing the task (accessible every time a task executes for the client)</li>
 * <li>Client new data: The data to be added to the client data pool after the task execution has completed</li>
 * <li>Shared data: The data pool accessible for all clients during task execution</li>
 * <li>Shared new data: The data to be added to the shared data pool after the task execution has completed</li>
 * <li>Database manager: Used to get a database connection to use during task execution</li>
 * </ul>
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class TaskExecutionEnvironment extends DynamicPropertyEntity
{
	private UUID clientId;
	private UUID packageId;

	private volatile TaskBase task;

	private volatile long requestTime;
	private volatile long executeTime;
	private volatile long completeTime;

	private volatile boolean success;
	private volatile boolean complete;

	private OwnzoneException exception;

	private PropertyBag resultBag;
	private PropertyBag clientDataBag;
	private PropertyBag clientNewDataBag;
	private PropertyBag sharedMemorySnapshot;
	private PropertyBag sharedMemoryAdditions;

	private DatabaseManager databaseManager;

	/**
	 * Instantiates a new task execution environment. This prepares all the fields and data bags for use.
	 */
	public TaskExecutionEnvironment()
	{
		this.requestTime = DateTimeHelper.getCurrentUnixTime();
		this.executeTime = 0L;
		this.completeTime = 0L;
		this.success = false;
		complete = false;

		resultBag = new PropertyBag();
		clientDataBag = new PropertyBag();
		clientNewDataBag = new PropertyBag();
		sharedMemorySnapshot = new PropertyBag();
		sharedMemoryAdditions = new PropertyBag();

		databaseManager = DatabaseManager.getDatabaseManagerInstance();
	}

	/**
	 * Finalize the environment object.
	 * <br>The purpose of this method is to allow the constructor to return before passing {@code this} to other parts of the system.
	 * <br>Because the object is not fully constructed <i>before</i> the constructor returns and this can cause problems.
	 *
	 * @param clientId the client id
	 * @param packageId the package id
	 * @param task the task
	 * @param data the data from the client request
	 */
	public void finalize(UUID clientId, UUID packageId, TaskBase task, ArrayList<DynamicProperty<?>> data)
	{
		this.clientId = clientId;
		this.packageId = packageId;
		this.task = task;
		if(task != null) this.task.copyProperties(data);
		if(task != null) this.task.setEnvironment(this); // don't pass "this" before constructor has returned
	}

	/**
	 * Gets the execution time (the timestamp at which the execution begun).
	 *
	 * @return the execute time
	 */
	public long getExecuteTime()
	{
		return executeTime;
	}

	/**
	 * Sets the execution time.
	 *
	 * @param executeTime the execution timestamp (the timestamp at which the execution begun).
	 */
	public void setExecuteTime(long executeTime)
	{
		this.executeTime = executeTime;
	}

	/**
	 * Gets the execution duration in milliseconds.
	 *
	 * @return the execution duration
	 */
	public long getExecuteDuration()
	{
		return (completeTime - executeTime);
	}

	/**
	 * Checks if the task is completed.
	 *
	 * @return true, if completed
	 */
	public boolean hasCompleted()
	{
		return getTask().hasCompleted() && complete;
	}

	/**
	 * Checks if the execution was successful (no exceptions were raised and the task execution returned {@code true})
	 *
	 * @return true, if successful
	 */
	public boolean success()
	{
		return success && exception == null;
	}

	/**
	 * Gets the exception raised during the execution, if any.
	 *
	 * @return the exception
	 */
	public OwnzoneException getException()
	{
		return exception;
	}

	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public UUID getClientId()
	{
		return clientId;
	}

	/**
	 * Gets the package id.
	 *
	 * @return the package id
	 */
	public UUID getPackageId()
	{
		return packageId;
	}

	/**
	 * Gets the task.
	 *
	 * @return the task
	 */
	public TaskBase getTask()
	{
		return task;
	}

	/**
	 * Gets the request timestamp.
	 *
	 * @return the request time
	 */
	public long getRequestTime()
	{
		return requestTime;
	}

	/**
	 * Gets result bag that holds all the data to be sent back to the client once the execution has completed.
	 *
	 * @return the result bag
	 */
	public PropertyBag getResultBag()
	{
		return resultBag;
	}

	/**
	 * Put client data before execution.
	 *
	 * @param client the client
	 */
	public void putClientData(DynamicPropertyEntity client)
	{
		clientDataBag.copyFromEntity(client);
	}

	/**
	 * Gets the client data bag. This holds all the data specific to the client.
	 *
	 * @return the client data bag
	 */
	public PropertyBag getClientDataBag()
	{
		return clientDataBag;
	}

	/**
	 * Gets the new client data bag. This holds the data to be added to the client's data pool after the execution has completed.
	 *
	 * @return the new client data bag
	 */
	public PropertyBag getNewClientDataBag()
	{
		return clientNewDataBag;
	}

	/**
	 * Gets the shared data snapshot. This holds the data accessible to all tasks during execution.
	 *
	 * @return the shared data snapshot
	 */
	public PropertyBag getSharedDataSnapshot()
	{
		return sharedMemorySnapshot;
	}

	/**
	 * Put shared memory snapshot before execution.
	 *
	 * @param propertyBag the property bag
	 */
	public void putSharedMemorySnapshot(DynamicPropertyEntity propertyBag)
	{
		sharedMemorySnapshot.copyFromEntity(propertyBag);
	}

	/**
	 * Gets the shared data additions. This data is put into the shared data pool, accessible by all tasks during execution.
	 *
	 * @return the shared data additions
	 */
	public PropertyBag getSharedDataAdditions()
	{
		return sharedMemoryAdditions;
	}

	/**
	 * Gets a database from all active connections.
	 *
	 * @param identifier the identifier
	 * @return the database connection
	 */
	public DatabaseConnection getDatabase(String identifier)
	{
		return databaseManager.getDatabaseConnection(identifier);
	}

	/**
	 * Complete a task.
	 *
	 * @param success the success
	 * @param exception the exception
	 */
	public void complete(boolean success, OwnzoneException exception)
	{
		this.completeTime = DateTimeHelper.getTimeStamp();
		this.success = success;
		this.exception = exception;
		this.complete = true; // Lock
	}

	/**
	 * Prepare a task for execution.
	 *
	 * @return the task put up for execution.
	 */
	public TaskBase prepareExecute()
	{
		executeTime = DateTimeHelper.getTimeStamp();
		return task;
	}
}
