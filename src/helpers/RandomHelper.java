package helpers;
import java.util.*;

/**
 * This utility class provides a number of methods dealing with the generation of random stuff. Cannot be instantiated; access statically.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class RandomHelper
{
	private static Random rng = new Random();

	private static String uppercases = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static String lowercases = "abcdefghijklmnopqrstuvwxyz";
	private static String numbers = "0123456789";
	private static String symbols = "!?+-_(){}[]%&$#@";

	private RandomHelper()
	{
	}

	/**
	 * Generates a random integer between {@code min} and {@code max}.
	 * 
	 * @param min
	 *            Minimum value
	 * @param max
	 *            Maximum value
	 * 
	 * @return A randomly generated value
	 * 
	 * @exception IllegalArgumentException
	 *                Thrown if there are problems with the arguments with detailed ExceptionMessage
	 */
	public static int randomInt(int min, int max)
	{
		if (min >= max)
		{
			throw new IllegalArgumentException("The min-value must not be greater than or equal to the max-value.");
		}

		return rng.nextInt((max - min) + 1) + min;
	}

	/**
	 * Rolls a six-sided dice.
	 * 
	 * @return The value of the dice roll
	 */
	public static int diceroll()
	{
		return randomInt(1, 6);
	}

	/**
	 * Reaches into the universe for an answer to help you decide.
	 * 
	 * @return "Yes" or "No", whichever the universe returns
	 */
	public static String decision()
	{
		return randomBoolean() ? "Yes" : "No";
	}

	/**
	 * Flips a coin.
	 * 
	 * @return "Heads" or "Tails"
	 */
	public static String coinflip()
	{
		return randomBoolean() ? "Heads" : "Tails";
	}

	/**
	 * Generates a series of random numbers.
	 * 
	 * @param length
	 *            The number of random numbers to generate, no greater than a million
	 * @param min
	 *            Minimum value
	 * @param max
	 *            Maximum value
	 * 
	 * @return A series of randomly generated values
	 * 
	 * @exception IllegalArgumentException
	 *                Thrown if there are problems with the arguments with detailed ExceptionMessage
	 */
	public static int[] randomSeries(int length, int min, int max)
	{
		if (length > 1000000)
		{
			throw new IllegalArgumentException("Length cannot be > 1 million");
		}

		int series[] = new int[length];

		for (int i = 0; i < length; i++)
		{
			series[i] = randomInt(min, max);
		}

		return series;
	}

	/**
	 * Generates a random boolean value.
	 * 
	 * @return {@code true} or {@code false}
	 */
	public static boolean randomBoolean()
	{
		return rng.nextBoolean();
	}

	/**
	 * Generates a random double value.
	 * 
	 * @return A double between {@code 0.0} and {@code 1.0}
	 */
	public static double randomDouble()
	{
		return rng.nextDouble();
	}

	/**
	 * Generates a UUID (Universally Unique IDentifier).
	 * 
	 * @return A freshly generated UUID
	 */
	public static UUID UUID()
	{
		return UUID.randomUUID();
	}

	/**
	 * Generates a random string with uppercases, lowercases, numbers and symbols.
	 * 
	 * @param length
	 *            The length of the string to generate, no greater than a million
	 * 
	 * @return A randomly generated string
	 * 
	 * @exception IllegalArgumentException
	 *                Thrown if there are problems with the arguments with detailed ExceptionMessage
	 */
	public static String randomString(int length)
	{
		if (length <= 0)
		{
			throw new IllegalArgumentException("Length cannot be <= 0.");
		}

		if (length > 1000000)
		{
			throw new IllegalArgumentException("Length cannot be > 1 million");
		}

		String characters = uppercases + lowercases + numbers + symbols;

		StringBuilder sb = new StringBuilder(length);

		for (int i = 0; i < length; i++)
		{
			sb.append(characters.charAt(randomInt(0, characters.length() - 1)));
		}

		return sb.toString();
	}

	/**
	 * Generates a random string according to the arguments.
	 * 
	 * @param length
	 *            The length of the string to generate, no greater than a million
	 * @param uppercases
	 *            Include uppercase letters
	 * @param lowercases
	 *            Include lowercase letters
	 * @param numbers
	 *            Include numbers
	 * @param symbols
	 *            Include symbols
	 * 
	 * @return A randomly generated string
	 * 
	 * @exception IllegalArgumentException
	 *                Thrown if there are problems with the arguments with detailed ExceptionMessage
	 */
	public static String randomString(int length, boolean uppercases, boolean lowercases, boolean numbers, boolean symbols)
	{
		String characters = constructCharactersString(uppercases, lowercases, numbers, symbols);

		StringBuilder sb = new StringBuilder(length);

		for (int i = 0; i < length; i++)
		{
			sb.append(characters.charAt(randomInt(0, characters.length() - 1)));
		}

		return sb.toString();
	}

	/**
	 * Generates a random serial.<br>
	 * Example: {@code A5T7-TSDX-XB6F-HM3G}
	 * 
	 * @param amount
	 *            The length of the string to generate, no greater than a million
	 * @param delimiter
	 *            The delimiter characters, for example "-"
	 * @param blocks
	 *            The amount of serial "blocks"; the above example has 4 blocks (max 10)
	 * @param blockLength
	 *            The length of each "block"; the above example has 4 characters per block (max 10)
	 * @param uppercases
	 *            Include uppercase letters
	 * @param lowercases
	 *            Include lowercase letters
	 * @param numbers
	 *            Include numbers
	 * @param symbols
	 *            Include symbols
	 * 
	 * @return A series of randomly generated serials
	 * 
	 * @exception IllegalArgumentException
	 *                Thrown if there are problems with the arguments with detailed ExceptionMessage
	 */
	public static String[] randomSerials(int amount, char delimiter, int blocks, int blockLength, boolean uppercases, boolean lowercases, boolean numbers, boolean symbols)
	{
		if (amount <= 0)
		{
			throw new IllegalArgumentException("You must inquire about more than 0 serials.");
		}

		if (amount > 1000000)
		{
			throw new IllegalArgumentException("No more than a million serials, please.");
		}

		if (blockLength <= 0)
		{
			throw new IllegalArgumentException("A block length of 0 is not possible.");
		}

		if (blockLength > 10)
		{
			throw new IllegalArgumentException("A blockLength of more than 10 is not allowed.");
		}

		if (blocks <= 0)
		{
			throw new IllegalArgumentException("0 or less blocks is simply not possible.");
		}

		if (blocks > 10)
		{
			throw new IllegalArgumentException("More than 10 blocks is a bit much.");
		}

		// Use this method to validate arguments
		constructCharactersString(uppercases, lowercases, numbers, symbols);

		String[] serials = new String[amount];

		for (int i = 0; i < amount; i++)
		{
			String serial = "";

			for (int j = 0; j < blocks; j++)
			{
				serial = serial + randomString(blockLength, uppercases, lowercases, numbers, symbols) + delimiter;
			}

			serials[i] = serial.substring(0, serial.length() - 1);
		}

		return serials;
	}

	private static String constructCharactersString(boolean uppercases, boolean lowercases, boolean numbers, boolean symbols)
	{
		String characters = "";

		if (uppercases)
		{
			characters += RandomHelper.uppercases;
		}

		if (lowercases)
		{
			characters += RandomHelper.lowercases;
		}

		if (numbers)
		{
			characters += RandomHelper.numbers;
		}

		if (symbols)
		{
			characters += RandomHelper.symbols;
		}

		if (characters.equals(""))
		{
			throw new IllegalArgumentException("You must include at least one type of characters.");
		}

		return characters;
	}

}
