package exceptions;

/**
 * The purpose of this class is to provide a singular, consistent exception management system for custom exceptions. <br>
 * This class can hold a custom message generated by the framework (or the developer) as well as the primary exception that was raised during runtime.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class OwnzoneException extends Exception
{
	private Exception primaryException;

	/**
	 * Instantiates a new core exception.
	 *
	 * @param message
	 *            the message describing the exception
	 * @param primaryException
	 *            the primary exception raised during runtime
	 */
	public OwnzoneException(String message, Exception primaryException)
	{
		super(message);
		this.primaryException = primaryException;
	}

	/**
	 * Instantiates a new core exception.
	 *
	 * @param message
	 *            the message describing the exception
	 */
	public OwnzoneException(String message)
	{
		super(message);
		this.primaryException = null;
	}

	/**
	 * Gets the primary exception.
	 *
	 * @return the primary exception
	 */
	public Exception getPrimaryException()
	{
		return primaryException;
	}

	/*
	 * Returns the custom message as well as the primary exception message.
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	public String getMessage()
	{
		if (primaryException == null) return "[ServerCore] " + super.getMessage();
		return "[ServerCore] " + super.getMessage() + ": " + primaryException.getMessage();
	}

	/**
	 * Gets only the message from the primary exception.
	 *
	 * @return the simple message
	 */
	public String getSimpleMessage()
	{
		return super.getMessage();
	}

}