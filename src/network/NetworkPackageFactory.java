package network;
import helpers.*;

import java.security.PublicKey;
import java.util.UUID;

import task.TaskExecutionEnvironment;
import util.DynamicProperty;
import exceptions.OwnzoneException;

/**
 * This statically accessed class helps building {@code NetworkPackage} objects according to their intended purpose.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class NetworkPackageFactory
{
	private final static String NAME = NetworkPackageFactory.class.getSimpleName();
	
	private NetworkPackageFactory()
	{

	}

	/**
	 * Builds a network package response to a client that failed for whichever reason the exception provides.
	 *
	 * @param exception the exception indicating the failure
	 * @param environment the task environment, containing extra data
	 * @return the network package
	 */
	public static NetworkPackage buildNetworkPackage(OwnzoneException exception, TaskExecutionEnvironment environment)
	{
		NetworkPackage networkPackage = new NetworkPackage();

		networkPackage.setClientId(environment.getClientId());
		networkPackage.setPackageId(environment.getPackageId());

		networkPackage.setException(exception);

		return networkPackage;
	}

	/**
	 * Builds a network package indicating a failure not related to task execution (connecting to the server, for example).
	 *
	 * @param exception the exception indicating the failure
	 * @param packageId the package id
	 * @return the network package
	 */
	public static NetworkPackage buildNetworkPackage(OwnzoneException exception, UUID packageId)
	{
		NetworkPackage networkPackage = new NetworkPackage();

		networkPackage.setPackageId(packageId);
		networkPackage.setException(exception);

		return networkPackage;
	}

	/**
	 * Builds a network package and adds the supplied parameters to it.
	 *
	 * @param properties the properties to be added
	 * @return the network package
	 */
	public static NetworkPackage buildNetworkPackage(DynamicProperty<?>... properties)
	{
		NetworkPackage networkPackage = new NetworkPackage();

		for (DynamicProperty<?> dynamicProperty : properties)
		{
			networkPackage.putProperty(dynamicProperty);
		}

		return networkPackage;
	}

	/**
	 * Builds a network package from a {@code TaskExecutionEnvironment}. This is typically used after a successful task execution.
	 *
	 * @param environment the environment, containing the data from the task execution
	 * @return the network package
	 */
	public static NetworkPackage buildNetworkPackage(TaskExecutionEnvironment environment)
	{
		NetworkPackage networkPackage = new NetworkPackage();

		networkPackage.setClientId(environment.getClientId());
		networkPackage.setPackageId(environment.getPackageId());

		networkPackage.copyFromEntity(environment.getResultBag());

		return networkPackage;
	}

	/**
	 * Builds the first message to the client, sent when a connection is successful.
	 *
	 * @param clientId the id of the client
	 * @param packageId the package id
	 * @return the network package
	 */
	public static NetworkPackage buildWelcomePacket(UUID clientId, UUID packageId)
	{
		try
		{
			NetworkPackage np;
			
			if(CryptoHelper.getClientPublicKey(clientId) != null)
			{
				np = buildNetworkPackage(new DynamicProperty<UUID>("ClientId", "", clientId, false), new DynamicProperty<PublicKey>("PublicKey", "", CryptoHelper.getPublicEncryptionKey(), true));
			}
			else
			{
				np = buildNetworkPackage(new DynamicProperty<UUID>("ClientId", "", clientId, false));
			}
			
			np.setPackageId(packageId);
			return np;
		}
		catch (OwnzoneException e)
		{
			ConsoleHelper.announce(NAME, e.getSimpleMessage());
			return buildNetworkPackage(e, packageId);
		}

	}
}