package server;

import helpers.*;

import java.io.*;
import java.net.*;
import java.security.PublicKey;
import java.util.UUID;

import network.*;
import statics.Settings;
import task.*;
import client.*;
import exceptions.OwnzoneException;


/**
 * The Server class' responsibilities are to act as a welcome socket for new clients, determining if clients may be accepted into the system or not, and delegating replies accordingly.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class Server
{
	private final String NAME = this.getClass().getSimpleName();

	private final int SERVER_PORT;
	private final int SERVER_TIMEOUT;
	private final String SERVER_PASSWORD;
	private final boolean ALLOW_MULTIPLE_INSTANCES;

	private ServerSocket serverSocket;
	private Socket connection;
	private ObjectOutputStream outStream;
	private ObjectInputStream inStream;

	private NetworkPackage networkPackage;

	private ClientManager clientManager;

	private boolean stop;

	public Server(ServerConfiguration serverConfiguration) throws OwnzoneException
	{
		Settings.loadSettings();
		
		SERVER_PORT = Settings.getPort();
		SERVER_TIMEOUT = Settings.getTimeout();
		SERVER_PASSWORD = Settings.getPassword();
		ALLOW_MULTIPLE_INSTANCES = Settings.isAllowMultipleInstances();
		
		stop = false;
		TaskExecutor.getTaskExecutorInstance().start();

		try
		{
			long timeStamp = System.currentTimeMillis();

			serverSocket = new ServerSocket(SERVER_PORT, 1);

			clientManager = ClientManager.getClientManagerInstance();

			TaskQueue tq = TaskQueue.getTaskQueueInstance();

			for (TaskBase tb : serverConfiguration.getAllTasks())
			{
				tq.registerTask(tb);
			}

			ConsoleHelper.announce(NAME, "Server started on port " + SERVER_PORT + " successfully. Timeout is " + SERVER_TIMEOUT + " ms.");
			ConsoleHelper.announce(NAME, "Construction took " + (System.currentTimeMillis() - timeStamp) + " ms.");
			serverLoop();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	 /**
	 * The server loop runs continuously throughout the life of the server and listens for new client connections.
	 */
	private void serverLoop()
	{
		ConsoleHelper.announce(NAME, "Entered server loop...");

		while (!stop)
		{
			ConsoleHelper.announce(NAME, "Waiting for client...");

			try
			{
				// Accept connection
				connection = serverSocket.accept();
				outStream = new ObjectOutputStream(connection.getOutputStream());
				inStream = new ObjectInputStream(connection.getInputStream());

				// Try to cast NetworkPackage
				networkPackage = (NetworkPackage) inStream.readObject();
				// System.out.println(networkPackage.getPropertyValue("test"));

				// The client ID must be null (or it's not a new client)
				if (networkPackage.getClientId() == null)
				{
					// Check the password to authenticate the client
					// TODO: Bake password into NetworkPackage from client automatically (before sending)
					if (networkPackage.getProperty("SERVER_PASSWORD").getValue().equals(SERVER_PASSWORD))
					{
						// Check if the client is already connected, if ALLOW_MULTIPLE_INSTANCES is false
						// TODO: Bake MachineID into NetworkPackage from client automatically (if ClientID is null)
						if (!ALLOW_MULTIPLE_INSTANCES)
						{
							Client client;
							if ((client = (Client) clientManager.getClientByMachineId((String) networkPackage.getPropertyValue("MACHINE_ID"))) != null)
							{
								declineClient(connection, outStream, inStream, "Client already connected.", networkPackage.getPackageId());
							}
							else
							{
								acceptClient(connection, outStream, inStream, networkPackage.getPackageId());
							}
						}
						else
						{
							acceptClient(connection, outStream, inStream, networkPackage.getPackageId());
						}
					}
					else
					{
						declineClient(connection, outStream, inStream, "Wrong password.", networkPackage.getPackageId());
					}
				}
				else
				{
					declineClient(connection, outStream, inStream, "Already got an ID.", networkPackage.getPackageId());
				}
			}
			catch (SocketTimeoutException e)
			{
				// e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (ClassNotFoundException e)
			{
				e.printStackTrace();
			}
			finally
			{
				// Cleanup
				connection = null;
				outStream = null;
				inStream = null;
			}
		}

		// TODO: Send shutdown message to all clients
	}

	 /**
	 * Registers the client in the system and returns the client's new ID to the client.
	 */
	private void acceptClient(Socket connection, ObjectOutputStream outStream, ObjectInputStream inStream, UUID packageId)
	{
		UUID clientId = RandomHelper.UUID();
		Client newClient = new Client(connection, outStream, inStream, clientId);

		newClient.putProperty(networkPackage.getProperty("MACHINE_ID"));

		CryptoHelper.addClientKey(clientId, (PublicKey) networkPackage.getPropertyValue("PUBLIC_KEY"));

		clientManager.addClient(newClient);

		reply(NetworkPackageFactory.buildWelcomePacket(clientId, packageId));
	}

	 /**
	 * Declines the client and replies with a reason.
	 */
	private void declineClient(Socket connection, ObjectOutputStream outStream, ObjectInputStream inStream, String reason, UUID packageId)
	{
		ConsoleHelper.announce(NAME, "Declined client: " + reason);

		reply(NetworkPackageFactory.buildNetworkPackage(new OwnzoneException(reason), packageId));
	}

	 /**
	 * Used to send a network package to the client from the welcome-socket.
	 * @param	networkPackage	The package to be sent to the client.
	 */
	private void reply(NetworkPackage networkPackage)
	{
		try
		{
			outStream.writeObject(networkPackage);
			outStream.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
