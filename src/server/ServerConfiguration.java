package server;

import java.io.IOException;
import java.util.ArrayList;

import org.ini4j.InvalidFileFormatException;

import statics.Settings;
import task.TaskBase;
import exceptions.OwnzoneException;

/**
 * The purpose of this class is to act as a primer for the {@link Server} class, providing necessary start-up information.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ServerConfiguration
{
	private ArrayList<TaskBase> tasks;
		
	/**
	 * Instantiates a new server configuration.
	 *
	 * @param appName the name of the application
	 */
	public ServerConfiguration(String appName) throws InvalidFileFormatException, IOException, OwnzoneException
	{
		Settings.setAppName(appName);

		tasks = new ArrayList<TaskBase>();
	}

	/**
	 * Adds a task to the configuration task list.
	 *
	 * @param task the task
	 */
	public void addTask(TaskBase task)
	{
		tasks.add(task);
	}

	/**
	 * Gets all tasks registered.
	 *
	 * @return all tasks
	 */
	public ArrayList<TaskBase> getAllTasks()
	{
		return tasks;
	}
}
