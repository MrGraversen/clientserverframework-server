package statics;
import java.io.*;
import java.net.*;

/**
 * This class provides some context for grabbing hardware and network information from the host computer.
 * <br>This class is used by the other components of the system to provide contextual helper information for their own methods.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ThisMachine
{
	private static String hwid = null;
	
	private static String globalIp = null;

	private ThisMachine()
	{

	}
	
	/**
	 * Gets the CPU id from the host computer. This identifier <i>should</i> be universally unique and provides ability to identify a physical machine.
	 * <br>Example of a CPU id: {@code BFEBFBFF000306C3}
	 *
	 * @return the CPU id of the computer
	 */
	public static String getCpuId()
	{
		if (hwid == null)
		{
			String result = "";

			try
			{
				File file = File.createTempFile("cpu", ".vbs");
				file.deleteOnExit();
				FileWriter fw = new java.io.FileWriter(file);

				String vbs = "" + "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n" + "Set colItems = objWMIService.ExecQuery(\"Select ProcessorId from Win32_Processor\")\n" + "For Each objItem in colItems\n" + "Wscript.Echo objItem.ProcessorId\n" + "Next";

				fw.write(vbs);
				fw.close();
				Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

				String line;
				while ((line = input.readLine()) != null)
				{
					result += line;
				}

				input.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			result = result.trim();

			hwid = result;
		}

		return hwid;
	}

	/**
	 * Gets the local network (IPv4) address.
	 *
	 * @return the local network address
	 */
	public static String getLocalNetworkAddress()
	{
		try
		{
			InetAddress ip = InetAddress.getLocalHost();
			return ip.getHostAddress();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * Gets the global network (IPv4) address.
	 *
	 * @return the global network address
	 */
	public static String getGlobalNetworkAddress()
	{
		if (globalIp == null)
		{
			try
			{
				URL ip = new URL("http://ownzone.org/ip/");
				URLConnection ipConnection = ip.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(ipConnection.getInputStream()));

				String inputLine = "";
				String ipLine = "";

				while ((inputLine = in.readLine()) != null)
					ipLine = inputLine;
				in.close();

				ipLine = ipLine.trim();
				globalIp = ipLine;
				return ipLine;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "";
			}
		}

		return globalIp;
	}

	/**
	 * Gets the Windows login username.
	 *
	 * @return the Windows username
	 */
	public static String getMachineUsername()
	{
		return System.getProperty("user.name");
	}

}
