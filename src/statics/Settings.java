package statics;

import helpers.*;

import java.io.IOException;

import org.ini4j.Wini;

import exceptions.OwnzoneException;

/**
 * This class handles all the settings read from the INI-file.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class Settings
{
	private static boolean read = false;
	private static String appName = "";

	// Settings below

	private static String port;
	private static String timeout;
	private static String password;
	private static String allowMultipleInstances;
	private static long launch;

	static
	{
		launch = DateTimeHelper.getCurrentUnixTime();
	}

	private Settings()
	{

	}

	public static String getAppName()
	{
		return appName;
	}

	public static void setAppName(String appName)
	{
		Settings.appName = appName;
	}

	public static void loadSettings() throws OwnzoneException
	{
		if (appName.equals("")) throw new OwnzoneException("The app must have a name, use 'setAppName(name)'");

		try
		{
			Wini ini;
			String path = FileHelper.getSettingsPath();
			ini = new Wini(FileHelper.readFile(path));

			port = ini.get("ServerSettings", "port");
			timeout = ini.get("ServerSettings", "timeout");
			password = ini.get("ServerSettings", "password");
			allowMultipleInstances = ini.get("ServerSettings", "allow_multiple_instances");

			// String test = ini.get("ServerSettings", "test");
			// System.out.println(test);

			read = true;
		}
		catch (IOException e)
		{
			throw new OwnzoneException("Error loading settings", e);
		}

	}

	public static boolean isSettingsLoaded()
	{
		return read;
	}

	public static int getPort()
	{
		return Integer.valueOf(port);
	}

	public static int getTimeout()
	{
		return Integer.valueOf(timeout);
	}

	public static String getPassword()
	{
		return password;
	}

	public static boolean isAllowMultipleInstances()
	{
		return Boolean.valueOf(allowMultipleInstances);
	}

	public static long getLaunchTime()
	{
		return launch;
	}

}
