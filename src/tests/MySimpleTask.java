package tests;

import exceptions.OwnzoneException;
import task.TaskBase;


public class MySimpleTask extends TaskBase
{
	public MySimpleTask()
	{
		super("MySimpleTask");
	}

	@Override
	protected boolean task() throws OwnzoneException
	{
		// This is what we'll look for
		System.out.println("MySimpleTask says hello!");
		
		// Return true to indicate successful execution
		return true;
	}

	@Override
	protected void beforeTask()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void afterTask()
	{
		// TODO Auto-generated method stub
		
	}
}
