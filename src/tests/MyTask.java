package tests;

import exceptions.OwnzoneException;
import task.TaskBase;

public class MyTask extends TaskBase
{
	
	public MyTask()
	{
		super("MyTask");
	}

	@Override
	protected boolean task() throws OwnzoneException
	{
		return true;
	}

	@Override
	protected void beforeTask()
	{
	}

	@Override
	protected void afterTask()
	{
	}
}
