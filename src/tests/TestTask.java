package tests;
import task.TaskBase;


public class TestTask extends TaskBase
{
	public TestTask(String identifier)
	{
		super(identifier);
	}

	@Override
	protected boolean task()
	{
		long iterations = (long) Math.pow(2, 26);
		long pi = 4;
		boolean plus = false;

		for (long i = 3; i < iterations; i += 2)
		{
			if (plus)
			{
				pi += 4.0 / i;
			}
			else
			{
				pi -= 4.0 / i;
			}

			plus = !plus;
		}

		return true;
	}

	@Override
	protected void beforeTask()
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected void afterTask()
	{
		// TODO Auto-generated method stub
	}

}
