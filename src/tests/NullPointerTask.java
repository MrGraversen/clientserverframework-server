package tests;

import exceptions.OwnzoneException;
import task.TaskBase;

public class NullPointerTask extends TaskBase
{
	public NullPointerTask()
	{
		super("NullPointerTask");
	}

	@Override
	protected boolean task() throws OwnzoneException
	{
		// Create a String reference (no object)
		String myString = null;
		
		// This will cause a NullPointerException in this case
		int length = myString.length();
		
		// This line will not be reached because of the exception
		return true;
	}

	@Override
	protected void beforeTask()
	{
		
	}

	@Override
	protected void afterTask()
	{
		
	}
}


