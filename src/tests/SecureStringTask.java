package tests;
import task.TaskBase;
import util.*;
import exceptions.OwnzoneException;
import helpers.CryptoHelper;


public class SecureStringTask extends TaskBase
{

	public SecureStringTask()
	{
		super("SecureStringTask");
	}

	@Override
	protected boolean task() throws OwnzoneException
	{
		// Get data
		SecureString encryptedString = getPropertyValue("SecureString");
		String plainString = getPropertyValue("PlaintText");
		
		// Decrypt encryptedString
		String decryptedString = encryptedString.getValue(CryptoHelper.getPrivateEncryptionKey());
		
		// Success indicates whether the decryption was successful or not
		return decryptedString.equals(plainString);
	}

	@Override
	protected void beforeTask()
	{
	}

	@Override
	protected void afterTask()
	{
	}

}


