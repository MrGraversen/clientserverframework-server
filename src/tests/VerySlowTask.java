package tests;

import exceptions.OwnzoneException;
import task.TaskBase;

public class VerySlowTask extends TaskBase
{
	public VerySlowTask()
	{
		super("VerySlowTask");
	}

	@Override
	protected boolean task() throws OwnzoneException
	{
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		return true;
	}

	@Override
	protected void beforeTask()
	{
		
	}

	@Override
	protected void afterTask()
	{
		
	}
	
	
}
