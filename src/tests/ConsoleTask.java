package tests;
import task.TaskBase;
import util.DynamicProperty;
import helpers.RandomHelper;

public class ConsoleTask extends TaskBase
{

	public ConsoleTask()
	{
		super("ConsoleTask");
	}

	@Override
	protected boolean task()
	{
		int sleep = RandomHelper.randomInt(1000, 10000);

		try
		{
			Thread.sleep(sleep);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("Wololo from " + getPropertyValue("name") + " (" + sleep + ")");

		//addResultData(new DynamicProperty<String>("wololo", "", "WOLOLOOO HEUHA HUU WOLOLOOO", false));
		addResultData(new DynamicProperty<String>("wololo", "", (String) getClientData().getPropertyValue("test_data"), false));
		addClientData(new DynamicProperty<String>(RandomHelper.randomString(10), "", RandomHelper.randomString(5), false));

		return true;
	}

	@Override
	protected void beforeTask()
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected void afterTask()
	{
		// TODO Auto-generated method stub

	}

}
