package client;
import helpers.*;

import java.util.UUID;

/**
 * This class builds sessions for clients, determining when their connections should expire.
 * <br>Cannot be instantiated; access staically.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class SessionFactory
{
	private SessionFactory()
	{

	}

	/**
	 * Builds a client session lasting 21600 seconds (6 hours).
	 *
	 * @return the client session
	 */
	public static ClientSession buildClientSession()
	{
		long obtained = DateTimeHelper.getCurrentUnixTime();
		long expiry = DateTimeHelper.getCurrentUnixTime() + 21600; // 6 hrs
		UUID id = RandomHelper.UUID();

		return new ClientSession(obtained, expiry, id);
	}
}
