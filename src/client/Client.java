package client;

import helpers.*;

import java.io.*;
import java.net.Socket;
import java.util.UUID;

import network.NetworkPackage;
import util.DynamicProperty;
import abstracts.*;
import exceptions.OwnzoneException;

/**
 * This class holds the connection to the client as well as hosts the listener and the client's session.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class Client extends DynamicPropertyEntity implements Manageable<UUID>
{
	private UUID clientId;

	private Socket connection;

	private ObjectOutputStream outStream;

	private ObjectInputStream inStream;

	private ClientSession clientSession;

	private ClientListener clientListener;

	private ClientManager clientManager;

	private long connectedAt;

	private long lastReset;

	/**
	 * Instantiates a new client using its connection variables and its id.
	 *
	 * @param connection
	 *            the socket connection
	 * @param outStream
	 *            the ObjectOutputStream of the client
	 * @param inStream
	 *            the ObjectInputStream of the client
	 * @param clientId
	 *            the client's identifier
	 */
	public Client(Socket connection, ObjectOutputStream outStream, ObjectInputStream inStream, UUID clientId)
	{
		super();

		this.lastReset = System.currentTimeMillis();

		putProperty(new DynamicProperty<String>("test_data", "Data for testing", RandomHelper.randomString(10), false));

		this.connection = connection;
		this.outStream = outStream;
		this.inStream = inStream;

		this.clientId = clientId;
		this.clientManager = ClientManager.getClientManagerInstance();

		clientSession = SessionFactory.buildClientSession();

		connectedAt = DateTimeHelper.getCurrentUnixTime();

		this.clientListener = new ClientListener(this);
		clientListener.start();
	}

	/**
	 * Gets the connected timestamp.
	 *
	 * @return the connected timestamp
	 */
	public long getConnectedTimestamp()
	{
		return connectedAt;
	}

	/**
	 * Gets the ObjectOutputStream for the client connection.
	 *
	 * @return the out stream
	 */
	public ObjectOutputStream getOutStream()
	{
		return outStream;
	}

	/**
	 * Gets the ObjectInputStream for the client connection.
	 *
	 * @return the in stream
	 */
	public ObjectInputStream getInStream()
	{
		return inStream;
	}

	/**
	 * Disconnect the client with an exception indicating the reason.
	 *
	 * @param exception
	 *            the exception
	 */
	public void disconnect(OwnzoneException exception)
	{
		clientListener.abort();
		clientListener.interrupt();
		clientManager.disconnect(this, exception);
	}

	/**
	 * Sends a reply to the client.
	 *
	 * @param networkPackage
	 *            the network package containing the reply data
	 */
	public void reply(NetworkPackage networkPackage)
	{
		try
		{
			outStream.writeObject(networkPackage);
			outStream.flush();

			if (System.currentTimeMillis() - lastReset > 10000)
			{
				lastReset = System.currentTimeMillis();
				outStream.reset();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see abstracts.Manageable#getIdentifier()
	 */
	@Override
	public UUID getIdentifier()
	{
		return clientId;
	}

	/**
	 * Gets the client session.
	 *
	 * @return the session
	 */
	public ClientSession getSession()
	{
		return clientSession;
	}

	public Socket getConnection()
	{
		return connection;
	}
}
