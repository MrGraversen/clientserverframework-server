package client;
import helpers.ConsoleHelper;

import java.io.IOException;
import java.util.*;

import util.*;
import abstracts.*;
import exceptions.OwnzoneException;

/**
 * This class handles all the client instances and provides utility to operate on the client base as well as the shared memory pool for all clients.
 * <br>This class is a singleton.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ClientManager extends Manager<UUID>
{
	private final String NAME = this.getClass().getSimpleName();

	private PropertyBag sharedMemoryPool;

	private static ClientManager clientManagerInstance = null;

	private ClientManager()
	{
		super();
		sharedMemoryPool = new PropertyBag();
		ConsoleHelper.announce(NAME, "Ready.");
	}

	/**
	 * Gets the singleton client manager instance.
	 *
	 * @return the client manager instance
	 */
	public static ClientManager getClientManagerInstance()
	{
		if (clientManagerInstance == null) clientManagerInstance = new ClientManager();

		return clientManagerInstance;
	}

	/**
	 * Adds a client to the system.
	 *
	 * @param client the client
	 */
	public void addClient(Client client)
	{
		putEntity(client);

		ConsoleHelper.announce(NAME, "Client " + client.getIdentifier().toString() + " added.");
		ConsoleHelper.announce(NAME, "Currently serving " + getNumberOfClientsFormatted());
	}

	/**
	 * Removes a client from the system.
	 *
	 * @param client the client to be removed
	 * @param exception the exception describing why the client was removed
	 */
	public void removeClient(Client client, OwnzoneException exception)
	{
		removeEntity(client);

		ConsoleHelper.announce(NAME, "Client " + client.getIdentifier().toString() + " removed. Reason: " + exception.getSimpleMessage());
		ConsoleHelper.announce(NAME, "Currently serving " + getNumberOfClientsFormatted());
	}

	/**
	 * Gets a list of all the clients registered in the system.
	 *
	 * @return all clients in an ArrayList
	 */
	public ArrayList<Client> getAllClients()
	{
		ArrayList<Client> listOfClients = new ArrayList<Client>();

		for (Manageable<UUID> m : getAllEntities())
		{
			listOfClients.add((Client) m);
		}

		return listOfClients;
	}

	/**
	 * Gets a client from its ID. Returns null if the client was not found.
	 *
	 * @param clientId the client's id
	 * @return the client (null if client doesn't exist)
	 */
	public Client getClient(UUID clientId)
	{
		return (Client) getEntity(clientId);
	}

	/**
	 * Gets the client by machine id (the id of the client's physical machine).
	 *
	 * @param machineId the machine id
	 * @return the client (null if client doesn't exist)
	 */
	public Client getClientByMachineId(String machineId)
	{
		for (Client client : getAllClients())
		{
			if (client.getPropertyValue("MACHINE_ID").equals(machineId))
			{
				return client;
			}
		}

		return null;
	}

	/**
	 * Gets the number of clients formatted.
	 *
	 * @return the number of clients formatted
	 */
	public String getNumberOfClientsFormatted()
	{
		int n = getNumberOfEntities();

		if (n == 1)
		{
			return n + " client.";
		}
		else if (n == 0)
		{
			return "no clients.";
		}
		else
		{
			return n + " clients.";
		}
	}

	/**
	 * Disconnect a client with an exception indicating why the client was disconnected.
	 *
	 * @param client the client to be disconnected
	 * @param exception the exception describing the reason
	 */
	public void disconnect(Client client, OwnzoneException exception)
	{
		try
		{
			client.getConnection().close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		removeClient(client, exception);
	}

	/**
	 * Adds data to the shared memory pool, accessible by all clients.
	 *
	 * @param propertyEntity the DynamicPropertyEntity containing the data to be added
	 */
	public void addSharedData(DynamicPropertyEntity propertyEntity)
	{
		for (DynamicProperty<?> dp : propertyEntity.getAllProperties())
		{
			sharedMemoryPool.putProperty(dp);
		}
	}

	/**
	 * Gets a data entry from the shared memory pool.
	 *
	 * @param identifier the identifier of the data entry
	 * @return the data
	 */
	public DynamicProperty<?> getSharedData(String identifier)
	{
		return sharedMemoryPool.getProperty(identifier);
	}

	/**
	 * Gets the shared memory pool.
	 *
	 * @return the shared data bag
	 */
	public PropertyBag getSharedDataBag()
	{
		return sharedMemoryPool;
	}
}