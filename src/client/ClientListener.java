package client;
import java.io.*;
import java.net.SocketException;

import network.NetworkPackage;
import task.TaskQueue;
import util.RequestType;
import exceptions.OwnzoneException;

/**
 * An instance of this class is attached to a client and listens continuously (throughout the life of the client) for network packages, in parallel, from the respective client it is listening for.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ClientListener extends Thread
{
	private final static int THROTTLE_VALUE = 20;
	
	private volatile boolean stop;
	
	private Client client;

	private boolean failing;
	
	private OwnzoneException exception;

	private TaskQueue taskQueue;

	/**
	 * Instantiates a new client listener.
	 *
	 * @param client the client hosting the listener
	 */
	public ClientListener(Client client)
	{
		this.client = client;
		taskQueue = TaskQueue.getTaskQueueInstance();
		stop = false;
		failing = false;
	}
	
	public void run()
	{
		while (!stop)
		{
			execute();
			hibernate(THROTTLE_VALUE);
		}
	}

	/**
	 * This method runs the main work of the listener, listening for network packages and determining what to do with the received package.
	 */
	private void execute()
	{
		try
		{
			NetworkPackage networkPackage = (NetworkPackage) client.getInStream().readObject();

			// Verify client
			if (networkPackage.getClientId().compareTo(client.getIdentifier()) == 0)
			{
				// Check session
				if (!client.getSession().isExpired())
				{
					switch (networkPackage.getRequestType())
					{
					case RequestType.GENERIC:
						if (!networkPackage.getTaskIdentifier().equals(""))
						{
							try
							{
								taskQueue.addTaskRequest(networkPackage.getClientId(), networkPackage.getPackageId(), networkPackage.getTaskIdentifier(), networkPackage.getAllProperties());
							}
							catch (OwnzoneException e)
							{
								e.printStackTrace();
								// Task was not found
							}
						}
						else
						{
							// No task requested
						}
						break;
					case RequestType.DISCONNECT:
						client.disconnect(new OwnzoneException("Intended disconnect by client"));
						break;

					default:
						break;
					}
				}
				else
				{
					// Session expired
					failing = true;
					exception = new OwnzoneException("Session expired");
				}
			}
			else
			{
				failing = true;
				exception = new OwnzoneException("Possible attempted identity theft");
				// Client not authenticated
			}
		}
		catch (SocketException | EOFException e)
		{
			failing = true;
			exception = new OwnzoneException("Disconnect", e);
		}
		catch (IOException e)
		{
			failing = true;
			exception = new OwnzoneException(e.getMessage(), e);
			// e.printStackTrace();
		}
		catch (ClassNotFoundException | ClassCastException e)
		{
			failing = true;
			exception = new OwnzoneException("Sending unrecognizable packets", e);
			// e.printStackTrace();
		}
		catch (Exception e)
		{
			failing = true;
			exception = new OwnzoneException("Unknown error", e);
			e.printStackTrace();
		}
		finally
		{
			if (failing)
			{
				client.disconnect(exception);
			}
		}
	}

	private void hibernate(int duration)
	{
		try
		{
			Thread.sleep((long) duration);
		}
		catch (InterruptedException e)
		{
			//abort();
		}
	}

	/**
	 * Aborts the listener, stopping the thread.
	 */
	public void abort()
	{
		stop = true;
		interrupt();
	}
}
