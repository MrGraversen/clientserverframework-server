package client;
import helpers.DateTimeHelper;

import java.util.UUID;

/**
 * The client's session object, determining if the client is still allowed to operate within the system.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ClientSession
{
	private long dateTimeObtained;
	
	private long dateTimeExpire;

	private UUID sessionId;

	/**
	 * Instantiates a new client session.
	 *
	 * @param dateTimeObtained the date time obtained (should almost always be the current timestamp)
	 * @param dateTimeExpire the date time expire (unix timestamp)
	 * @param sessionId the session id
	 */
	public ClientSession(long dateTimeObtained, long dateTimeExpire, UUID sessionId)
	{
		super();
		this.dateTimeObtained = dateTimeObtained;
		this.dateTimeExpire = dateTimeExpire;
		this.sessionId = sessionId;
	}

	/**
	 * Gets the date and time for when the session object was created formatted in {@code DD-MM-YYYY} format.
	 *
	 * @return the date and time
	 */
	public String getDateTimeObtainedFormatted()
	{
		return DateTimeHelper.getDate(dateTimeObtained);
	}

	/**
	 * Gets the date and time for when the session object is expiring formatted in {@code DD-MM-YYYY} format.
	 *
	 * @return the date time expire formatted
	 */
	public String getDateTimeExpireFormatted()
	{
		return DateTimeHelper.getDate(dateTimeExpire);
	}

	/**
	 * Gets the timestamp (unix time) for when the session was created.
	 *
	 * @return the date time obtained
	 */
	public long getDateTimeObtained()
	{
		return dateTimeObtained;
	}

	/**
	 * Gets the timestamp (unix time) for when the session is expiring.
	 *
	 * @return the date time expire
	 */
	public long getDateTimeExpire()
	{
		return dateTimeExpire;
	}

	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public UUID getSessionId()
	{
		return sessionId;
	}

	/**
	 * Checks if the session is expired.
	 *
	 * @return true, if is expired
	 */
	public boolean isExpired()
	{
		return DateTimeHelper.getCurrentUnixTime() > dateTimeExpire;
	}

}
