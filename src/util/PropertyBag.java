package util;
import task.TaskExecutionEnvironment;
import abstracts.DynamicPropertyEntity;

/**
 * This class is a simple implementation of the {@link DynamicPropertyEntity} that is used to hold data for a {@link TaskExecutionEnvironment}.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class PropertyBag extends DynamicPropertyEntity
{
	
	/**
	 * Instantiates a new property bag.
	 */
	public PropertyBag()
	{

	}
}
