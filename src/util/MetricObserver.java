package util;

import helpers.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import statics.Settings;

/**
 * This singleton class collects metric data about the application and builds a HTML document from this data, to make the metric data easily digestible for the developer.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class MetricObserver
{
	private final String NAME = this.getClass().getSimpleName();

	private ArrayList<String> tasks;
	private ConcurrentHashMap<String, Integer> taskExecutes;
	private ConcurrentHashMap<String, ArrayList<Long>> taskTimings;
	private ConcurrentHashMap<UUID, Integer> clientTaskRequests;
	private ConcurrentHashMap<UUID, ArrayList<String>> clientTasks;

	private static MetricObserver metricObserverInstance;

	private long logUpdate;

	/**
	 * Instantiates the class, preparing its resources for use.
	 */
	private MetricObserver()
	{
		// clientManager = ClientManager.getClientManagerInstance();
		tasks = new ArrayList<>();
		taskExecutes = new ConcurrentHashMap<String, Integer>();
		taskTimings = new ConcurrentHashMap<String, ArrayList<Long>>();
		clientTaskRequests = new ConcurrentHashMap<UUID, Integer>();
		clientTasks = new ConcurrentHashMap<UUID, ArrayList<String>>();
		logUpdate = 0L;

		ConsoleHelper.announce(NAME, "Ready.");
	}

	/**
	 * Gets the singleton task queue instance.
	 *
	 * @return the metric observer instance
	 */
	public static MetricObserver getMetricObserverInstance()
	{
		if (metricObserverInstance == null) metricObserverInstance = new MetricObserver();

		return metricObserverInstance;
	}

	/**
	 * This method builds a HTML document from static HTML, using metric data collected during the life cycle of the application. This also takes a maximum-250 entry of the regular log and adds it to the document for easy access. The log is limited to prevent this method's execution time from getting out of hand.
	 */
	public void generateHtmlMetrics()
	{
		long timeStamp = System.currentTimeMillis();

		logUpdate = DateTimeHelper.getTimeStamp();

		StringBuilder sb = getTemplateDocument();

		String content = sb.toString();

		content = content.replace("<var:title>", "Server Core Log");

		content = content.replace("<var:serverName>", Settings.getAppName());

		String logContent = "";
		ArrayList<String> logEntries = new ArrayList<>(ConsoleHelper.getInstanceLog());

		if (logEntries.size() > 250) logEntries = new ArrayList<>(logEntries.subList(logEntries.size() - 250, logEntries.size()));

		for (String logEntry : logEntries)
		{
			logEntry = logEntry.replace("\u2014", "&mdash;");
			logContent = logContent + logEntry + "<br>";
		}

		content = content.replace("<var:logContent>", logContent);

		content = content.replace("<var:logTitle>", "Log since " + DateTimeHelper.getTime(Settings.getLaunchTime()) + " (" + logEntries.size() + " entries)");

		String taskTableContent = "";
		for (String t : tasks)
		{
			int taskCount = taskExecutes.get(t);
			int taskTiming = getAverageRunDurationForTask(t);

			taskTableContent = taskTableContent + "<tr><td>" + t + "</td><td>" + taskCount + "</td><td>" + taskTiming + " ms</td></tr>";
		}

		content = content.replace("<var:taskTable>", taskTableContent);

		String clientTableContent = "";
		for (UUID clientId : clientTaskRequests.keySet())
		{
			ArrayList<String> taskExecutesList = clientTasks.get(clientId);
			int requestCount = clientTaskRequests.get(clientId);

			clientTableContent = clientTableContent + "<tr><td>" + clientId + "</td><td>" + requestCount + "</td>";

			String frequencyString = "";
			for (String taskName : tasks)
			{
				int freq = Collections.frequency(taskExecutesList, taskName);
				if(freq > 0) frequencyString = frequencyString + taskName + " (" + freq + ")<br>";
			}

			clientTableContent = clientTableContent + "<td>" + frequencyString + "</td></tr>";
		}
		
		content = content.replace("<var:clientTable>", clientTableContent);

		content = content.replace("<var:buildTime>", "Built in " + (System.currentTimeMillis() - timeStamp) + " ms.");

		writeHtmlToFile(content);
	}

	/**
	 * Reads the template document to allow constructing the HTML log.
	 *
	 * @return a StringBuilder containing the content of the template file
	 */
	private StringBuilder getTemplateDocument()
	{
		StringBuilder contentBuilder = new StringBuilder();

		try
		{
			BufferedReader in = new BufferedReader(new FileReader(FileHelper.getAppDirectory() + "\\html-template.html"));

			String str;
			while ((str = in.readLine()) != null)
			{
				contentBuilder.append(str + "\n");
			}

			in.close();
		}
		catch (IOException e)
		{
			ConsoleHelper.announce(NAME, "Template file not found, adding it.");
			FileHelper.fixTemplateFile();
		}

		return contentBuilder;
	}

	/**
	 * Writes the constructed HTML to the HTML log.
	 *
	 * @param s
	 *            the HTML content to be written to the HTML log
	 */
	private void writeHtmlToFile(String s)
	{
		File f = new File(FileHelper.getAppDirectory() + "\\html-log.html");

		try
		{
			FileWriter fwriter = new FileWriter(f);
			BufferedWriter bwriter = new BufferedWriter(fwriter);
			bwriter.write(s);
			bwriter.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Calculates the average execution time for a given task.
	 *
	 * @param taskIdentifier
	 *            the task identifier
	 * @return the average run duration for task
	 */
	private int getAverageRunDurationForTask(String taskIdentifier)
	{
		if (taskExecutes.get(taskIdentifier) == 0) return 0;

		long sum = 0;

		for (long l : taskTimings.get(taskIdentifier))
		{
			sum += l;
		}

		return (int) (sum / taskExecutes.get(taskIdentifier));
	}

	/**
	 * Determines whether or not the log should be generated.
	 *
	 * @return true, if log should generate
	 */
	public boolean shouldLogUpdate()
	{
		int interval = 5000; // TODO: from settings

		return logUpdate + interval <= DateTimeHelper.getTimeStamp();
	}

	/**
	 * Adds a task to be observed by the MetricObserver.
	 *
	 * @param taskIdentifier
	 *            the task identifier
	 */
	public void addObservedTask(String taskIdentifier)
	{
		tasks.add(taskIdentifier);
		taskExecutes.put(taskIdentifier, 0);
		taskTimings.put(taskIdentifier, new ArrayList<Long>());
	}

	/**
	 * Updates the metrics for a specific task, incrementing the number of times it has executed, and registers its execution duration.
	 *
	 * @param taskIdentifier
	 *            the task identifier
	 * @param runDuration
	 *            the run duration
	 */
	public void updateTaskMetrics(String taskIdentifier, int runDuration)
	{
		try
		{
			taskExecutes.put(taskIdentifier, taskExecutes.get(taskIdentifier) + 1);
			taskTimings.get(taskIdentifier).add((long) runDuration);
			// taskTimings.put(taskIdentifier, taskTimings.get(taskIdentifier) + (long) runDuration);
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Updates the metrics for which tasks a client has requested, and how many times the tasks have been requested.
	 *
	 * @param clientId
	 *            the client id
	 * @param taskIdentifier
	 *            the task identifier
	 */
	public void updateClientTaskRequests(UUID clientId, String taskIdentifier)
	{
		try
		{
			if (!clientTaskRequests.containsKey(clientId)) clientTaskRequests.putIfAbsent(clientId, 0);

			clientTaskRequests.put(clientId, clientTaskRequests.get(clientId) != null ? clientTaskRequests.get(clientId) + 1 : 0);

			ArrayList<String> listOfTasks = clientTasks.get(clientId);

			if (listOfTasks == null)
			{
				listOfTasks = new ArrayList<>();
			}

			listOfTasks.add(taskIdentifier);

			clientTasks.put(clientId, listOfTasks);
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
	}
}